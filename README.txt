
FEATURES EXPORT LANGUAGE
----------------

This module switches language to Drupal's basic default language, which is exclusively English, when exporting, recreating or updating a Feature.
Usually if a features is getting exported, the language of the site is used. That means that in special cases (f.e. views names) you get a translated string.
But following Drupal's coding standards (http://drupal.org/coding-standards) and best practices in general, all names in code should be English and later beeing translated
through Drupal's locale system. Since Features are Modules, we actually have to/should export English strings only.

This works via UI and drush command.


CREDITS
-------

Developed by Felix Delattre (xamanu) <drupal@delattre.de>
Development sponsored by Erdfisch <http://www.erdfisch.de/>

