<?php

/**
 * @file
 * Module switching to English language when exporting, recreating or updating any feature.
 */

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_export_language_pre_features_update() {
  _features_export_language_set();
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_export_language_pre_features_update_all() {
  _features_export_language_set();
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_export_language_pre_features_export() {
  _features_export_language_set();
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_export_language_pre_features_diff() {
  _features_export_language_set();
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_export_language_pre_features_list() {
  _features_export_language_set();
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_export_language_pre_features_revert() {
  _features_export_language_set();
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_export_language_pre_features_revert_all() {
  _features_export_language_set();
}
